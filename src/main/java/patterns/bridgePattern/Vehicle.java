package patterns.bridgePattern;

import java.util.ArrayList;
import java.util.List;

public abstract class Vehicle {

    // assempbly line for the workshops
    List<WorkShop> workshops = new ArrayList<>();


    public Vehicle() {
        super();
    }

    boolean joinWorkshop(WorkShop workshop) {
        return workshops.add(workshop);
    }

    public abstract void manufacture();

    public abstract int minWorkTime();

}
