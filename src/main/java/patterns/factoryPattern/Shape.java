package patterns.factoryPattern;

public interface Shape {
    public void draw();
}
