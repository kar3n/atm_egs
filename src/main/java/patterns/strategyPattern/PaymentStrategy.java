package patterns.strategyPattern;

public interface PaymentStrategy {

    public void pay(int amount);
}
