package exceptions;

public class WrongBank extends Exception {
    public WrongBank(String message) {
        super(message);
    }
}
