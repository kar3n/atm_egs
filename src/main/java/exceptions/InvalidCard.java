package exceptions;

public class InvalidCard extends Exception {
    public InvalidCard(String message) {
        super(message);
    }
}
