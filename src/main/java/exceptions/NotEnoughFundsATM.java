package exceptions;

public class NotEnoughFundsATM extends Exception {
    public NotEnoughFundsATM(String message) {
        super(message);
    }
}
