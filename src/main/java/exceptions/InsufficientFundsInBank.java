package exceptions;

public class InsufficientFundsInBank extends Exception {
    public InsufficientFundsInBank(String message) {
        super(message);
    }
}
