package model;

import enums.CardType;
import enums.Currency;

import java.time.Instant;


public final class Card {

    private final long cardNumber;
    private final Currency currency;
    private final Customer customer;
    private final Instant expireDate;
    private final CardType type;
    private final String issuerBank;

    public Card(long cardNumber, Currency currency, Customer customer, Instant expireDate, CardType type, String issuerBank) {
        this.cardNumber = cardNumber;
        this.currency = currency;
        this.expireDate = expireDate;
        this.customer = new Customer(customer.getFirstName(), customer.getLastName());
        this.type = type;
        this.issuerBank = issuerBank;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Customer getCustomer() {
        return new Customer(customer.getFirstName(), customer.getLastName());
    }

    public Instant getExpireDate() {
        return expireDate;
    }

    public CardType getType() {
        return type;
    }

    public String getIssuerBank() {
        return issuerBank;
    }

    public static class Builder{
        private long cardNumber;
        private Currency  currency;
        private Customer customer;
        private Instant expireDate;
        private CardType type;
        private String issuerBank;

        public Builder() {
        }

        public Builder setCardNumber(long cardNumber) {
            this.cardNumber = cardNumber;
            return this;
        }

        public Builder setCurrency(Currency currency) {
            this.currency = currency;
            return this;
        }

        public Builder setCustomer(Customer customer) {
            this.customer = customer;
            return this;
        }

        public Builder setExpireDate(Instant expireDate) {
            this.expireDate = expireDate;
            return this;
        }

        public Builder setType(CardType type) {
            this.type = type;
            return this;
        }

        public Builder setIssuerBank(String issuerBank) {
            this.issuerBank = issuerBank;
            return this;
        }

        public Card buildCard()  {
            return new Card(cardNumber, currency, customer, expireDate, type, issuerBank);
        }
    }

    @Override
    public String toString() {
        return "Card: ---" +
                "Card Number: " + cardNumber +
                " Customer: " + customer.getFirstName() + " " + customer.getLastName() +
                " Currency: " + currency +
                " Expire Date: " + expireDate.toString() +
                " Type: " + type +
                " " + issuerBank +
                " ---";
    }
}
