package model;

import interfaces.Ibank;
import org.xml.sax.SAXException;
import service.DataBaseConnector;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Map;

public class HSBC implements Ibank {

    private final String databasePath = "src\\main\\resources\\hsbcAccounts.xml";
    private Map<Long, Account> ameriaBankCards;

    public HSBC() {
        try {
            ameriaBankCards = DataBaseConnector.newInstance().parse(databasePath, "HSBC");
        } catch (IOException | SAXException | ParserConfigurationException e) {
            e.printStackTrace();
        }
    }



    public Map<Long, Account> getBankCards() {
        return ameriaBankCards;
    }

    @Override
    public boolean checkCardExistance(Card card) {
        return getBankCards().get(card.getCardNumber()) != null && getBankCards().get(card.getCardNumber()).getCustomer().getLastName().equals(card.getCustomer().getLastName());
    }

    @Override
    public boolean checkCardBalance(long cardNumber, long cashAmount) {
        Account details = getBankCards().get(cardNumber);
        return details.getBalance() >= cashAmount;
    }

    @Override
    public void withdraw(Card card, long amount) {
        long currentBalance = getBankCards().get(card.getCardNumber()).getBalance();
        getBankCards().get(card.getCardNumber()).setBalance(currentBalance - amount);
    }

}
