package model;

import java.util.Random;

public class Account {
    private long balance;
    private long accNumber;
    private Customer customer;

    public Account(Customer customer) {
        balance = 500000;
        Random rand = new Random();
        accNumber = (long) rand.nextInt(3000001);
        this.customer = customer;
    }

    public long getBalance() {
        return balance;
    }

    public long getAccNumber() {
        return accNumber;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public Customer getCustomer() {
        return customer;
    }

    @Override
    public String toString() {
        return "Account number: " + accNumber +
                " Balance: " + balance;
    }
}
