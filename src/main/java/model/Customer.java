package model;

public class Customer {

    private String firstName;
    private String lastName;

    public Customer(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    @Override
    public boolean equals(Object obj) {
        Customer cust = (Customer) obj;
        if (this.getFirstName().equals(cust.getFirstName()) && this.getLastName().equals(cust.getLastName()))
            return true;
        return false;
    }
}
