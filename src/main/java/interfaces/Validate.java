package interfaces;


import model.Card;

public interface Validate {
    boolean isValid(Card card);

}
