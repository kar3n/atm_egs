package interfaces;


import model.Card;

public interface Ibank {

    boolean checkCardExistance(Card card);
    boolean checkCardBalance(long cardNumber, long cashAmount);
    void withdraw(Card card, long amount);

}
