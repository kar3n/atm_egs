package enums;

import interfaces.Ibank;
import model.AEB;
import model.AmeriaBank;
import model.HSBC;
import model.InecoBank;

public enum IssuerBank {

    AMERIA("AmeriaBank"){
        Ibank bank;
        @Override
        public Ibank getBank() {
            if (bank == null)
                bank = new AmeriaBank();
            return bank;
        }
    },
    HSBC("HSBC"){
        Ibank bank;

        @Override
        public Ibank getBank() {
            if (bank == null)
                bank = new HSBC();
            return bank;
        }
    },
    INECO("InecoBank"){
        Ibank bank;

        @Override
        public Ibank getBank() {
            if (bank == null)
                bank = new InecoBank();
            return bank;
        }
    },
    AEB("AEB"){
        Ibank bank;

        @Override
        public Ibank getBank() {
            if (bank == null)
                bank = new AEB();
            return bank;
        }
    };

    String name;

    IssuerBank(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Ibank getBankByName(String name){
        for (IssuerBank b : IssuerBank.values()) {
            if (b.name.equalsIgnoreCase(name))
                return b.getBank();
        }
        return null;
    }
    public abstract Ibank getBank();
}
