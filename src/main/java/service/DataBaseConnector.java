package service;

import enums.CardType;
import enums.Currency;
import model.Account;
import model.Card;
import model.Customer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class DataBaseConnector {

    private static DataBaseConnector parser;

    private DataBaseConnector() {
    }

    public static DataBaseConnector newInstance() {
        if (parser == null) {
            parser = new DataBaseConnector();
        }
        return parser;
    }

    public Map<Long, Account> parse(String filePath, String issuerName) throws IOException, SAXException, ParserConfigurationException {

        Map<Long, Account> cards = new HashMap<>();
        Document doc = getDocument(new File(filePath));
        NodeList nList = doc.getElementsByTagName("card");

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                Card card = new Card.Builder()
                .setCardNumber(Long.parseLong(eElement.getElementsByTagName("cardNumber").item(0).getTextContent()))
                .setType(CardType.getByName(eElement.getElementsByTagName("cardBrand").item(0).getTextContent()))
                .setCurrency(Currency.valueOf(eElement.getElementsByTagName("currency").item(0).getTextContent()))
                .setCustomer(new Customer(eElement.getElementsByTagName("name").item(0).getTextContent(), eElement.getElementsByTagName("surname").item(0).getTextContent()))
                .setExpireDate(Instant.parse(eElement.getElementsByTagName("expireDate").item(0).getTextContent()))
                .setIssuerBank(issuerName)
                .buildCard();

                cards.put(card.getCardNumber(), new Account(card.getCustomer()));
            }
        }

        return cards;
    }


    private Document getDocument(File file) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = builderFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(file);
        doc.getDocumentElement().normalize();
        return doc;
    }
}
