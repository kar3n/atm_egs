package service;


import enums.IssuerBank;
import exceptions.InsufficientFundsInBank;
import exceptions.InvalidCard;
import exceptions.WrongBank;
import interfaces.Ibank;
import model.Card;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ARCA
{
	private static Logger LOG = Logger.getLogger(ARCA.class.getName());
	
	public static boolean manageRequest(Card card, long cashAmount) throws InterruptedException
	{
		
		Ibank bank = IssuerBank.getBankByName(card.getIssuerBank());
		
		LOG.log(Level.INFO, "Starting ARCA");
		Thread.sleep(1100);
		if (bank == null)
		{
			try
			{
				throw new WrongBank("Wrong Bank Instance");
			}
			catch (WrongBank wrongBank)
			{
				LOG.log(Level.SEVERE, "Wrong Bank Issuer: {0}", new Object[]{card.getIssuerBank()});
				return false;
			}
		}
		else if (bank.checkCardExistance(card))
		{
			LOG.log(Level.INFO, "Card account exists: {0}", new Object[]{card.getCardNumber()});
			Thread.sleep(1100);
			if (bank.checkCardBalance(card.getCardNumber(), cashAmount))
			{
				LOG.log(Level.INFO, "Bank Account Balance is OK");
				Thread.sleep(1100);
				LOG.log(Level.INFO, "Withdrawing requested amount: {0}", new Object[]{cashAmount});
				Thread.sleep(1100);
				bank.withdraw(card, cashAmount);
				LOG.log(Level.INFO, "Done!");
				return true;
			}
			else
			{
				try
				{
					throw new InsufficientFundsInBank("Low on cash");
				}
				catch (InsufficientFundsInBank insufficientFundsInBank)
				{
					LOG.log(Level.INFO, "Insufficient funds in bank account for card: {0}", new Object[]{card.getCardNumber()});
					return false;
				}
			}
		}
		else
		{
			try
			{
				throw new InvalidCard("No such card");
			}
			catch (InvalidCard invalidCard)
			{
				LOG.log(Level.SEVERE, "Card doesn't exist: {0}", new Object[]{card});
				return false;
			}
		}
	}
}
