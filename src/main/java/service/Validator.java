package service;


import exceptions.InvalidCard;
import exceptions.InvalidDate;
import interfaces.Validate;
import model.Card;

import java.time.Instant;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Validator implements Validate {

    private static Logger LOG = Logger.getLogger(Validator.class.getName());

    @Override
    public boolean isValid(Card card) {
        LOG.log(Level.INFO, "Starting to validate card in ATM");
        try {
            Thread.sleep(1100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean temp;
        int length = 16;

        if (String.valueOf(card.getCardNumber()).length() == length){
            temp = true;
        }
        else {
            try {
                throw new InvalidCard("Wrong card number");
            } catch (InvalidCard invalidCard) {
                LOG.log(Level.SEVERE, "Card number is not valid: {0}", new Object[]{card.getCardNumber()});
            }
            temp = false;
        }

        LocalDate ld = LocalDate.now();
        if (card.getExpireDate().compareTo(Instant.now()) > 0){
            temp = true;
        }
        else {
            try {
                throw new InvalidDate("Invalid card, expired date");
            } catch (InvalidDate invalidDate) {
                LOG.log(Level.SEVERE, "Card expire date is passed {0}", new Object[]{card.getExpireDate()});
            }
            temp = false;
        }
        return temp;
    }
}
