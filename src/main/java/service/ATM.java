package service;


import exceptions.NotEnoughFundsATM;
import interfaces.Validate;
import model.Card;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ATM {
    private static Logger LOG = Logger.getLogger(ATM.class.getName());
    private static long balance = 1500000;
    private static Validate validator = new Validator();


    public ATM() {
    }

    public static void setBalance(long balance) {
        ATM.balance = balance;
    }

    public static boolean isEnoughFunds(long inputAmount) throws InterruptedException {
        LOG.log(Level.INFO, "Checking ATM balance");
        Thread.sleep(1100);
        if (inputAmount > balance){
            try {
                throw new NotEnoughFundsATM("ATM is currently not working, try later");
            } catch (NotEnoughFundsATM notEnoughFundsATM) {
                LOG.log(Level.INFO, "Not enough funds in ATM, amount requested: {0}, ATM cash amount: {1}", new Object[]{inputAmount, balance});
            }
            return false;
        }
        return true;
    }

    public static void start(Card card, long requestedAmount) throws InterruptedException {
        if (validator.isValid(card)){
            LOG.log(Level.INFO, "Validation Succeed");
            Thread.sleep(1100);
            if (isEnoughFunds(requestedAmount)){
                LOG.log(Level.INFO, "ATM Balance is OK");
                Thread.sleep(1100);
                LOG.log(Level.INFO, "Requesting ARCA service");
                Thread.sleep(1100);
                if (ARCA.manageRequest(card, requestedAmount))
                    balance -= requestedAmount;
            }
            else System.exit(0);
        }
        else System.exit(0);
    }

}
