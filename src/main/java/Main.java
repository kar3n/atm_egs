import enums.CardType;
import enums.Currency;
import model.Card;
import model.Customer;
import service.ATM;

import java.time.Instant;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        Card card = new Card.Builder()
                .setCustomer(new Customer("Ashot", "Hovhannisyan"))
                .setCardNumber(5503362547895417L)
                .setCurrency(Currency.RUB)
                .setExpireDate(Instant.parse("2023-12-03T10:15:30.00Z"))
                .setIssuerBank("AmeriaBank")
                .setType(CardType.VISA)
                .buildCard();



        ATM.start(card, 600000);
    }
}
